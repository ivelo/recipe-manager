import os
import requests
import json
import datetime 
import shutil 

class Manager:
    def __init__(
        self,
        api_host = '',
    ):
        """
        Manager class for db interaction
        """
        self.api_host = 'http://10.128.124.243:8003'
        self.storage = '/calib/velo/.recipe/'

    def _post(self, apath, data):
        session = requests.Session()
        session.trust_env = False
        content = session.post(
            "{}{}".format(self.api_host, '/api/' + apath),
            data = json.dumps(data),
            verify = False,
        )
        content.raise_for_status()
        return content.json()

    def _get(self, apath):
        session = requests.Session()
        session.trust_env = False
        content = session.get(
            "{}{}".format(self.api_host, '/api/' + apath)
        )
        content.raise_for_status()
        return content.json()

    def hw2json(self, path):
        f =  open(path, 'r')
        line = f.readline()
        hwdict = {}
        while line:
            if line != '\n':
                hwdict[line.split('=')[0].split('.')[1]] = int(line.split('=')[1]) #TODO: remove hardcoding the line number
            line = f.readline()
        print(hwdict)
        return json.dumps(hwdict)

    def matrix_count_mask(self, *, path) -> int:
        f = open(path, 'r')
        line = f.readline().split('=')[1]
        accumulated: int = 0
        while line:
            mask1, mask2 = 0, 0
            for i in range(128):
                chunk = line[i*3: i*3 +3]
                #print(chunk[1], format(int(chunk[1], 16), '04b'))
                
                mask1 += int(format(int(chunk[0], 16), '04b')[1])
                mask2 += int(format(int(chunk[1], 16), '04b')[3])
                #print(i, chunk, chunk[0], chunk[1], mask1, mask2)
            accumulated += mask1 + mask2
            line = f.readline().split('=')[-1]
            #print(accumulated)
        return accumulated
    
    def fname2id(self, path):
        return int(path[6:8]), int(path[11]), int(path[13])

    def ismodulein(self, mod, list) -> bool:
        for item in list:
            if mod in item:
                return True
        return False

    def list_hw(
        self,
        task: dict = None
    ):
        response = self._get('recipe/hw')
        print(response)
        #return response

    def upload_hw(
        self, 
        task: dict = None
    ):
        ### TODO: Move to some sort of initialise (or install)
        if not os.path.isdir(self.storage + 'VeloPix/'):
            os.mkdir(self.storage + 'VeloPix/')
        if not os.path.isdir(self.storage + 'VeloPix/Chip/'):
            os.mkdir(self.storage + 'VeloPix/Chip/')
        ######
        rname = task['path'].rstrip('/').split('/')[-1]
        t = datetime.datetime.now()

        data = {'date': t.strftime('%H:%M:%S %d-%m-%Y'),
                'kind': rname,
                'uploaded_by': os.getlogin(),
                'uploaded_from': os.uname().nodename, 
                'comment': task['comment']}
        #POST the recipe
        self._post('recipe/hw', data)

        dpath = self.storage + 'VeloPix/Chip/' + t.strftime('%Y-%m-%d-%H-%M-%S')
        os.mkdir(dpath)
        dpath += '/' + rname + '/'
        os.mkdir(dpath)
        
        #add new HwRecipe
        #add new HwSubRecipes for filepaths in task['which'],
        #link HwSubRecipes from previous HwRecipe for all other filepaths
        for mod in range(52):
            if task['which'] == None or self.ismodulein(str(mod), task['which']):
                os.mkdir(dpath + 'Module' + str(mod).zfill(2) + '/')
            for tile in range(4):
                for asic in range(3):
                    fname = 'Module' + str(mod).zfill(2) + '_VP' + str(tile) + '-' + str(asic) + '.dat'
                    fpath = task['path'] + '/Module' + str(mod).zfill(2) + '/' + fname
                    if task['which'] == None or fname in task['which']: 
                        #POST the subrecipe
                        data = {'date': t.strftime('%H:%M:%S %d-%m-%Y'),
                                'recipe': rname, 
                                'asic': asic + 3*tile + 12*mod,
                                'data': self.hw2json(fpath),
                                'fpath': dpath + 'Module' + str(mod).zfill(2) + '/' + fname}
                        print('Making POST', data) 
                        self._post('recipe/subrecipe/hw', data)
                        #Save to the file storage
                        shutil.copy2(fpath, dpath + 'Module' + str(mod).zfill(2) + '/')
                    else:
                        #LINK the subrecipe
                        data = {'recipe': rname, 
                                'asic': asic + 3*tile + 12*mod}
                        print('Making LINK', data)
                        self._post('recipe/subrecipe/hw/link', data)

    ### The matrix recipe 09.04.2024
    def upload_matrix(
        self, 
        task: dict = None
    ):
        ### TODO: Move to some sort of initialise (or install)
        if not os.path.isdir(self.storage + 'VeloPix/'):
            os.mkdir(self.storage + 'VeloPix/')
        if not os.path.isdir(self.storage + 'VeloPix/Matrix/'):
            os.mkdir(self.storage + 'VeloPix/Matrix/')
        ######
        rname = task['path'].rstrip('/').split('/')[-1]
        t = datetime.datetime.now()

        data = {'date': t.strftime('%H:%M:%S %d-%m-%Y'),
                'kind': rname,
                'uploaded_by': os.getlogin(),
                'uploaded_from': os.uname().nodename, 
                'comment': task['comment']}
        #POST the recipe
        self._post('recipe/matrix', data)

        dpath = self.storage + 'VeloPix/Matrix/' + t.strftime('%Y-%m-%d-%H-%M-%S')
        os.mkdir(dpath)
        dpath += '/' + rname + '/'
        os.mkdir(dpath)
        
        #add new MatrixRecipe
        #add new MatrixSubRecipe for filepaths in task['which'],
        #link MatrixSubRecipe from previous MatrixRecipe for all other filepaths
        for mod in range(52):
            if task['which'] == None or self.ismodulein(str(mod), task['which']):
                os.mkdir(dpath + 'Module' + str(mod).zfill(2) + '/')
            for tile in range(4):
                for asic in range(3):
                    fname = 'Module' + str(mod).zfill(2) + '_VP' + str(tile) + '-' + str(asic) + '_pixels.dat'
                    fpath = task['path'] + '/Module' + str(mod).zfill(2) + '/' + fname
                    if task['which'] == None or fname in task['which']: 
                        #POST the subrecipe
                        data = {'date': t.strftime('%H:%M:%S %d-%m-%Y'),
                                'recipe': rname, 
                                'asic': asic + 3*tile + 12*mod,
                                'masks': self.matrix_count_mask(path = fpath),
                                'fpath': dpath + 'Module' + str(mod).zfill(2) + '/' + fname}
                        print('Making POST', data)
                        self._post('recipe/subrecipe/matrix', data)
                        #Save to the file storage
                        shutil.copy2(fpath, dpath + 'Module' + str(mod).zfill(2) + '/')
                    else:
                        #LINK the subrecipe
                        data = {'recipe': rname, 
                                'asic': asic + 3*tile + 12*mod}
                        print('Making LINK', data)
                        self._post('recipe/subrecipe/matrix/link', data)

    def upload_prbs(
        self, 
        task: dict = None
    ):
        rname = task['path'].rstrip('/').split('/')[-1]
        t = datetime.datetime.now()

        data = {'date': t.strftime('%H:%M:%S %d-%m-%Y'),
                'name': rname,
                'uploaded_by': os.getlogin(),
                'uploaded_from': os.uname().nodename, 
                'comment': task['comment']}
        #POST the recipe
        self._post('prbs', data)
        
        print('Moving to POST the prbs data...')
        for mod in range(52):
            print('... Module' + str(mod).zfill(2))
            fname = 'M' + str(mod).zfill(2)
            #print(mod, os.path.isfile(task['path'] + fname))
            if not os.path.isfile(task['path'] + '/' + fname):
                print('... ... file missing');
                for i in range(20):
                    data = {'module': mod}
                    self._post('prbs/links', data)
                continue
            f = open(task['path'] + '/' + fname, 'r')
            line = f.readline()
            while line:
                data = {'link': int(line.split(':')[0].split(' ')[1]),
                        'module': mod,
                        'nbits': int(line.split(':')[1].split(' ')[1]),
                        'error': int(line.split(':')[1].split(' ')[2]),}
                #print('Making POST', data)
                self._post('prbs/links', data)
                line = f.readline()
            

                        
