import argparse
from manager import Manager

if __name__ == '__main__':
    desc = """
    A runnable script for recipe management
    """

    parser = argparse.ArgumentParser(description = desc)
    subparser = parser.add_subparsers(help = 'Available options')

    upload_parser = subparser.add_parser('upload', help = 'upload to db')
    upload_parser.set_defaults(task = 'upload')
    upload_parser.add_argument('recipe', action = 'store', choices = ['hw', 'matrix'],
                        help = 'which recipe type to upload')
    upload_parser.add_argument('path', action = 'store',
                        help = 'absolute path to the recipe, including the recipe dir')
    upload_parser.add_argument('comment', action = 'store')
    upload_parser.add_argument('--which', '-w', action = 'store', const = None, default = None, nargs = '*',
                        help = 'a list of local paths to the modified files')
    

    prbs_parser = subparser.add_parser('prbs', help = 'upload prbs to db')
    prbs_parser.set_defaults(task = 'prbs')
    #prbs_parser.add_argument('errors', action = 'store', choices = ['hw'],
    #                    help = 'which recipe type to upload')
    prbs_parser.add_argument('path', action = 'store',
                        help = 'absolute path to the prbs results, including the dir')
    prbs_parser.add_argument('comment', action = 'store')
    #prbs_parser.add_argument('--which', '-w', action = 'store', const = None, default = None, nargs = '*',
    #                    help = 'a list of local paths to the modified files')

    list_parser = subparser.add_parser('list', help = 'list the recipies')
    list_parser.set_defaults(task = 'list')

    args = parser.parse_args()
    manager = Manager()
    print(args.__dict__)
    if args.__dict__['task'] == 'upload':
        if args.__dict__['recipe'] == 'hw':
            manager.upload_hw(task = args.__dict__)
        if args.__dict__['recipe'] == 'matrix':
            manager.upload_matrix(task = args.__dict__)
    if args.__dict__['task'] == 'list':
        manager.list_hw(task = args.__dict__)
    if args.__dict__['task'] == 'prbs':
        manager.upload_prbs(task = args.__dict__)